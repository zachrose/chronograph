require('should');
var assert = require('assert');
var sinon = require('sinon');
var Timer = require('../index');

describe('Timer', function(){

    afterEach(function(){
        console.log.restore && console.log.restore();
        console.time.restore && console.time.restore();
        console.timeEnd.restore && console.timeEnd.restore();
    });

    it('is instantiated with a label', function(){
        t = new Timer('foo');
        t.should.be.ok;
    });

    it('wraps console.time', function(){
        sinon.stub(console, 'time');
        t = new Timer('bar');
        assert(console.time.calledOnce);
    });

    it('wraps console.timeEnd', function(done){
        sinon.stub(console, 'timeEnd');
        t = new Timer('baz');
        process.nextTick(function(){
            t.end();
            assert(console.timeEnd.calledOnce);
            console.timeEnd.restore();
            done();
        });
    });

    it('logs the time from instantiation to #end()', function(done){
        // digs into node.js console implementation details, :-\
        sinon.stub(console, 'log');
        t = new Timer('quz');
        setTimeout(function(){
            t.end();
            assert(console.log.calledOnce);
            console.log.getCall(0).args[2].should.be.within(95,200);
            console.log.restore();
            done();
        }, 100);
    });

    it('keeps different timers with the same label from colliding', function(done){
        t1 = new Timer('fud');
        t2 = new Timer('fud');
        sinon.stub(console, 'log');
        setTimeout(function(){
            t1.end();
        }, 200);
        setTimeout(function(){
            t2.end();
        }, 300);
        setTimeout(function(){
            assert(console.log.calledTwice);
            console.log.getCall(0).args[2].should.be.within(200,300);
            console.log.getCall(1).args[2].should.be.within(300,400);
            console.log.restore();
            done();
        }, 600);
    });
});
