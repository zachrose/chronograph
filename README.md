# Chronograph

This is a tiny wrapper around [console.time](http://nodejs.org/api/stdio.html#stdio_console_time_label) that adds a random identifier to your label.

Why do you want this? You want this because a Node.js process will handle many HTTP requests at once, and calls to console.time('foo') and console.endTime('foo') are not isolated on a per-request basis. As an example:

* Alice makes a request to '/'
* Your app calls console.time('root')
* Some asynchronous things happen to serve Alice's request.
* Bob makes a request to '/'
* Your app makes another call to console.time('root')
* The asynchronous things from Alice's request are done and callbacks are called.
* Your app calls console.endTime('root')

You wanted to time how long it took to serve Alice's request. What you got instead was some irrelevant duration between the beginning of Bob's request and the end of Alice's.

This library attempts to solve that problem by keeping a randomly generated id for every call to console.time and console.timeEnd.

Use it like this:

```
Chronograph = require('chronograph');

chronograph = new Chronograph('my great label');
// 153ms later...
chronograph.end();
// logged: "my great label (gzosz8vx2f) 153ms"
```
