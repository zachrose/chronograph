"use strict";

function Timer(label){
    var id = (Math.random()+1).toString(36).substr(2,10),
    uniqueLabel = label+' ('+id+')';
    console.time(uniqueLabel);
    this.end = function(){
        console.timeEnd(uniqueLabel);
    }
    return this;
}

module.exports = Timer;
